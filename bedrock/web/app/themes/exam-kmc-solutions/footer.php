            <div id="footer" class="wp-block-group footer">
                <div class="wp-block-group__inner-container">
                    <div class="wp-block-columns">
                        <div class="wp-block-column">
                            <?php if(is_active_sidebar('footer_left')): ?>
                                <div class="footer-logo">
                                    <?php dynamic_sidebar('footer_left'); ?>
                                </div>
                            <?php endif; ?>
                        </div>
                        <div class="wp-block-column">
                            <?php if(is_active_sidebar('footer_right')): ?>
                                <div class="footer-copy">
                                    <?php dynamic_sidebar('footer_right'); ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <?php wp_footer(); ?>
    </body>
</html>