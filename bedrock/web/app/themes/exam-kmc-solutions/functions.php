<?php
add_theme_support('custom-logo');
add_theme_support('menus');
add_theme_support('widgets');

function import_scripts(){
    wp_register_style('bootstrap-css', get_template_directory_uri() . '/css/bootstrap.min.css');
    wp_enqueue_style('bootstrap-css');

    wp_register_style('bootstrap-theme', get_template_directory_uri() . '/css/bootstrap-theme.min.css');
    wp_enqueue_style('bootstrap-theme');

    wp_register_style('google-font-1', 'https://fonts.googleapis.com/css2?family=Work+Sans:wght@300;400;600&display=swap');
    wp_enqueue_style('google-font-1');

    wp_register_style('google-font-2', 'https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;700&display=swap');
    wp_enqueue_style('google-font-2');

    wp_register_style('style', get_template_directory_uri() . '/style.css');
    wp_enqueue_style('style');

    wp_register_script('jquery', get_template_directory_uri() . '/js/vendor/jquery-1.11.2.min.js');
	wp_enqueue_script('jquery');

    wp_register_script('modernizr-js', get_stylesheet_directory_uri() . '/js/vendor/modernizr-2.8.3.min.js');
	wp_enqueue_script('modernizr-js');

    wp_register_script('bootstrap-js', get_template_directory_uri() . '/js/vendor/bootstrap.min.js');
	wp_enqueue_script('bootstrap-js');

    wp_register_script('fontawesome', 'https://use.fontawesome.com/00ac137d79.js');
	wp_enqueue_script('fontawesome');

    wp_register_script('custom-js', get_template_directory_uri() . '/js/script.js');
	wp_enqueue_script('custom-js');
}
add_action('wp_enqueue_scripts', 'import_scripts');

function get_theme_logo(){
    $custom_logo_id = get_theme_mod('custom_logo');
    $image = wp_get_attachment_image_src($custom_logo_id , 'full');
    return $image[0];
}

function custom_widgets_init(){
	register_sidebar(
		array(
			'name'          => 'Footer Left',
			'id'            => 'footer_left',
			'description'   => '',
			'before_widget' => '<div id="footer-left">',
			'after_widget'  => '</div>',
			'before_title'  => '',
			'after_title'   => '',
		)
	);
    
	register_sidebar(
		array(
			'name'          => 'Footer Right',
			'id'            => 'footer_right',
			'description'   => '',
			'before_widget' => '<div id="footer-right">',
			'after_widget'  => '</div>',
			'before_title'  => '',
			'after_title'   => '',
		)
	);
}
add_action('widgets_init', 'custom_widgets_init');