<?php global $post; ?>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
        <?php wp_head(); ?>
    </head>
    <body <?php body_class('page-id-'.$post->ID); ?>>
        <?php wp_body_open(); ?>

        <nav class="navbar" role="navigation">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div id="website-logo">
                            <a href="<?php home_url(); ?>" title="<?php echo get_bloginfo('name'); ?>"><img src="<?php echo get_theme_logo(); ?>" /></a>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <a href="#menu" class="menu-toggle"><i class="fa fa-bars"></i></a>
                        <?php wp_nav_menu('main-menu'); ?>
                    </div>
                </div>
            </div>
        </nav>

        <main>