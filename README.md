## How to install?
1. Pull all the files from bitbucket
2. Then, place the folder **bedrock** inside **//xampp/htdocs/** (for XAMPP users)
3. Import the database file. Get the file here **/bedrock/web/db_kmc.sql**
4. Update the hosts file (DRIVE:\Windows\System32\drivers\etc) with the following:

	127.0.0.1 localhost

	127.0.0.1 exam.kmc
	

5. For code checking, you can find the theme files here: **/bedrock/web/app/themes/exam-kmc-solutions**
6. Use below login details to get inside wp-admin:

	user: exam

	pass: password123